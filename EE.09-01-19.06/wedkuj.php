<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wędkujemy</title>
    <link rel="stylesheet" href="styl_1.css">
</head>
<body>
    <section class="baner">
        <h1>Portal dla wędkarzy</h1>
    </section>
    <section class="lewy">
        <h2>Ryby drapieżne naszych wód</h2>
        <?php
            $conn = mysqli_connect('localhost', 'root', '', 'ee.09-01-19');
            $sql = 'SELECT `nazwa`, `wystepowanie` FROM `ryby` WHERE `styl_zycia` = 1';
            $wynik = mysqli_query($conn, $sql);
            echo '<ul>';

            while($row = mysqli_fetch_array($wynik)){
                echo "<li>".$row['nazwa'].", występowanie: ".$row['wystepowanie']."</li>";
            }

            echo '</ul>';
            mysqli_close($conn);
        ?>
    </section>
    <section class="prawy">
        <img src="ryba1.jpg" alt="Sum"><br>
        <a href="kwerendy.txt">Pobierz kwerendy</a>
    </section>
    <section class="stopka">
        <p>Stronę wykonał: MAJLO</p>
    </section>


</body>
</html>
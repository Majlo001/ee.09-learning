<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Klub wędkowania</title>
    <link rel="stylesheet" href="styl2.css">
</head>
<body>
    <div class="baner">
        <h2>Wędkuj z nami!</h2>
    </div>
    <div class="lewy">
        <img src="ryba2.jpg" alt="Szczupak">
    </div>
    <div class="prawy">
        <h3>Ryby spokojnego żeru (białe)</h3>
        <?php
            $conn = mysqli_connect('localhost','root','','wedkowanie2');
            $sql = "SELECT `id`, `nazwa`, `wystepowanie` FROM `ryby` WHERE `styl_zycia` = 2;";
            $wynik = mysqli_query($conn, $sql);
            while($row = mysqli_fetch_array($wynik)){
                echo $row["id"].". ".$row["nazwa"].", występuje w: ".$row["wystepowanie"]."<br>";
            }
            mysqli_close($conn);

        ?>
        <br>
        <ol>
            <li><a href="https://wedkuje.pl/" target="_blank">Odwiedź także</a></li>
            <li><a href="http://www.pzw.org.pl/" target="_blank">Polski Związek Wędkarski</a></li>
        </ol>
    </div>

    <div class="stopka">
        <p>Stronę wykonał: MAJLO</p>
    </div>

</body>
</html>